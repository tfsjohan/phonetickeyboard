//
//  AppDelegate.h
//  PhoneticKeyboard
//
//  Created by Johan Nordberg on 2015-01-14.
//  Copyright (c) 2015 Digitalt Hjärta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

