//
//  KeyboardViewController.m
//  PhoneticKeyboardExtension
//
//  Created by Johan Nordberg on 2015-01-14.
//  Copyright (c) 2015 Digitalt Hjärta. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "KeyboardViewController.h"
#import "KeyboardView.h"

@interface KeyboardViewController () 
@property (nonatomic, strong) UIButton *nextKeyboardButton;
@end

@implementation KeyboardViewController {
    KeyboardView *keyboardView;
    CFURLRef soundFileURLRef;
    SystemSoundID soundFileObject;
}

-(BOOL)enableInputClicksWhenVisible {
    NSLog(@"Reading input click");
    return YES;
}

- (void)updateViewConstraints {
    [super updateViewConstraints];
    
    // Add custom view sizing constraints here
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UINib *nib = [UINib nibWithNibName:@"Portrait-iPhone" bundle:nil];
    keyboardView = (KeyboardView *)[nib instantiateWithOwner:self options:nil][0];
    
    [self.view addSubview:keyboardView];
    
    self.view.backgroundColor = keyboardView.backgroundColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated
}

- (void)textWillChange:(id<UITextInput>)textInput {
    // The app is about to change the document's contents. Perform any preparation here.
}

- (void)textDidChange:(id<UITextInput>)textInput {
    // The app has just changed the document's contents, the document context has been updated.
    
    UIColor *textColor = nil;
    if (self.textDocumentProxy.keyboardAppearance == UIKeyboardAppearanceDark) {
        textColor = [UIColor whiteColor];
    } else {
        textColor = [UIColor blackColor];
    }
    [self.nextKeyboardButton setTitleColor:textColor forState:UIControlStateNormal];
}

-(IBAction) onKeyDown: (UIButton *) sender {
    NSString *text = [self keyForButton: sender];
    [self playSound: text];
}

-(IBAction) onKeyUp: (UIButton *) sender {
    NSString *text = [self keyForButton: sender];
    [self.textDocumentProxy insertText:text];
}

-(IBAction) nextKeyboard: (id) sender {
    [self advanceToNextInputMode];
}

-(NSString *) keyForButton: (UIButton *) button {
    if(button.tag > 0) {
        return [NSString stringWithFormat:@"%c", (int)button.tag];
    } else {
        return button.titleLabel.text;
    }
}

-(void) playSound: (NSString *) letter {
    CFBundleRef mainBundle = CFBundleGetMainBundle ();
    
    soundFileURLRef  = CFBundleCopyResourceURL (
                                                mainBundle,
                                                CFSTR ("a_le_sv_se"),
                                                CFSTR ("aiff"),
                                                NULL
                                                );
    
    AudioServicesCreateSystemSoundID (soundFileURLRef,&soundFileObject);
    
    AudioServicesPlaySystemSound (soundFileObject);
    
    NSLog(@"%i", soundFileObject);
}
@end
